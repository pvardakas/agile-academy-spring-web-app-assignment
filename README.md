# Agile Academy Spring Web App Assignment

This is my final assignment of developing a web application using Java and Spring Boot. The assignment’s purpose is to serve as a capstone project for the various Spring (Boot) elements  including Spring Core, Data, Rest, Spring MVC and Security.

## Goals

The assignment’s purpose is to serve as a capstone project for the various Spring (Boot) elements that were taught in the context of Agile Academy’s Java and Spring course, including Spring Core, Spring Data, Spring Rest, Spring MVC and Spring Security.

- Modelling of the domain into domain classes using JPA/Hibernate mappings
- Connecting and Interacting with the DB using Spring Data
- Developing Rest Service applications and communication amongst them
- Authentication/Authorization using spring security
- Securing the exposed application endpoints using Spring security

## Functionalities
- When an unregistered user visits the homepage, they see the list of movies that have been added to the system, sorted by publication date. This list can be sorted by the number of likes​, number of dislikes or the publication date​. The default sorting is by publication date descending from the most recent to the oldest ones.
- Each item on the list should display the movie title, movie description, the name of the user that posted it, the date of publication, the number of likes and the number of dislikes.
- The name of the user can be a clickable link that when clicked upon, filters the movies down to the movies that have been shared by this specific user sorted by publication date. This list can also be sorted by the number of likes​, number of dislikes or the publication date​
- In addition to the functionality described above, registered users are able to add their own movies using a form and express their opinion for other movies by either a like​ or a dislike. Voting is performed by clicking on the respective links, or counters, that are displayed for each movie.
- When an unregistered user visits the homepage, there should be links for logging in or signing up. Once a user logs in his name should be displayed.
- Unregistered users should be able to log into their account or sign up for a new one
- Unregistered users should not be able to add a new movie or vote for a movie


## Technical Requirements
- Java SE 11
- Spring Boot, Data, JPA, Rest,  MVC, Security
- Database: MySQL or H2
- Maven 
- Freemarker template for views

## License
[MIT](https://choosealicense.com/licenses/mit/)