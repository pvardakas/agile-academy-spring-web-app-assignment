package com.learningactors.academy.amdb.service

import com.learningactors.academy.amdb.converter.EntityConverter
import com.learningactors.academy.amdb.converter.MovieConverter
import com.learningactors.academy.amdb.converter.UserConverter
import com.learningactors.academy.amdb.domain.Movies
import com.learningactors.academy.amdb.dto.MovieDto
import com.learningactors.academy.amdb.entity.Movie
import com.learningactors.academy.amdb.enums.SortingOrder
import com.learningactors.academy.amdb.enums.SortingType
import com.learningactors.academy.amdb.repository.MovieRepository
import org.springframework.data.domain.Sort
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate

class MovieServiceImplSpec extends Specification {
    private MovieRepository movieRepository = Mock(MovieRepository)
    private MovieConverter movieConverter = Mock(MovieConverter)
    private UserService userService = Mock(UserService)
    private UserConverter userConverter = Mock(UserConverter)
    private EntityConverter entityConverter = Mock(EntityConverter)
    private MovieServiceImpl movieServiceImpl
    private Long id1, id2
    private LocalDate date1, date2
    private int likes1, likes2
    private int dislikes1, dislikes2
    private Movie movie1, movie2
    private static List<Movie> listOfMoviesASC
    private static List<Movie> listOfMoviesDESC
    private MovieDto movieDto1, movieDto2
    private static List<MovieDto> listOfDtosASC
    private static List<MovieDto> listOfDtosDESC
    private Sort.Direction asc = Sort.Direction.ASC
    private Sort.Direction desc = Sort.Direction.DESC


    def setup() {
        movieServiceImpl = new MovieServiceImpl(movieRepository,
                movieConverter, userService, userConverter, entityConverter
        )
        id1 = 1L
        id2 = 2L
        date1= LocalDate.of(2020, 1, 1)
        date2 = LocalDate.of(2020, 1, 2)
        likes1 = 10
        likes2 = 20
        dislikes1 = 10
        dislikes2 = 20

        movie1 = new Movie()
        movie1.id >> id1
        movie1.createdAt >> date1
        movie1.likes >> likes1
        movie1.dislikes >> dislikes1

        movie2 = new Movie()
        movie2.id >> id2
        movie2.createdAt >> date2
        movie2.likes >> likes2
        movie2.dislikes >> dislikes2

        listOfMoviesASC = [movie1, movie2]
        listOfMoviesDESC = [movie2, movie1]

        movieDto1 = new MovieDto()
        movieDto1.id >> id1
        movieDto1.publicationDate >> date1
        movieDto1.likes >> likes1
        movieDto1.dislikes >> dislikes1

        movieDto2 = new MovieDto()
        movieDto2.id >> id2
        movieDto2.publicationDate >> date2
        movieDto2.likes >> likes2
        movieDto2.dislikes >> dislikes2

        listOfDtosASC = [movieDto1, movieDto2]
        listOfDtosDESC = [movieDto2, movieDto1]
        asc = Sort.Direction.ASC
        desc = Sort.Direction.DESC
    }

    def "should return a movie from repository given the movie id"() {
        given:
        movieRepository.findMovieById(id1) >> movie1

        when:
        Movie result = movieServiceImpl.findMovieById(id1)

        then:
        result == movie1
    }

    def "should return a list of movies order by date in descent order given null as sorting type and order"() {
        given:
        SortingType byDate = SortingType.DATE

        when:
        Movies moviesReturn = movieServiceImpl.findMovies(sortingType, sortingOrder)

        then:
        1 * movieRepository.findAll(Sort.by(desc, byDate.getValue())) >> listOfMoviesDESC
        1 * entityConverter.ToDto(listOfMoviesDESC) >> listOfDtosDESC
        0 * _

        and:
        moviesReturn.movies.size() == 2
        moviesReturn.movies == listOfDtosDESC

        where:
        sortingType                 |sortingOrder
        null                        |null
    }

    def "should return a list of movies order by date in descent order"() {
        given:
        SortingType orderBy = SortingType.DATE

        when:
        Movies moviesReturn = movieServiceImpl.findMovies(sortingType, sortingOrder)

        then:
        1 * movieRepository.findAll(Sort.by(desc, orderBy.getValue())) >> listOfMoviesDESC
        1 * entityConverter.ToDto(listOfMoviesDESC) >> listOfDtosDESC
        0 * _

        and:
        moviesReturn.movies.size() == 2
        moviesReturn.movies == listOfDtosDESC

        where:
        sortingType                 |sortingOrder
        SortingType.DATE            |SortingOrder.DESC
    }

    def "should return a list of movies order by date in ascent order"() {
        given:
        SortingType orderBy = SortingType.DATE

        when:
        Movies moviesReturn = movieServiceImpl.findMovies(sortingType, sortingOrder)

        then:
        1 * movieRepository.findAll(Sort.by(asc, orderBy.getValue())) >> listOfMoviesASC
        1 * entityConverter.ToDto(listOfMoviesASC) >> listOfDtosASC
        0 * _

        and:
        moviesReturn.movies.size() == 2
        moviesReturn.movies == listOfDtosASC

        where:
        sortingType                 |sortingOrder
        SortingType.DATE            |SortingOrder.ASC
    }

    def "should return a list of movies order by Likes in descent order"() {
        given:
        SortingType orderBy = SortingType.LIKES

        when:
        Movies moviesReturn = movieServiceImpl.findMovies(sortingType, sortingOrder)

        then:
        1 * movieRepository.findAll(Sort.by(desc, orderBy.getValue())) >> listOfMoviesDESC
        1 * entityConverter.ToDto(listOfMoviesDESC) >> listOfDtosDESC
        0 * _

        and:
        moviesReturn.movies.size() == 2
        moviesReturn.movies == listOfDtosDESC

        where:
        sortingType                 |sortingOrder
        SortingType.LIKES          |SortingOrder.DESC
    }

    def "should return a list of movies order by Likes in ascent order"() {
        given:
        SortingType orderBy = SortingType.LIKES

        when:
        Movies moviesReturn = movieServiceImpl.findMovies(sortingType, sortingOrder)

        then:
        1 * movieRepository.findAll(Sort.by(asc, orderBy.getValue())) >> listOfMoviesASC
        1 * entityConverter.ToDto(listOfMoviesASC) >> listOfDtosASC
        0 * _

        and:
        moviesReturn.movies.size() == 2
        moviesReturn.movies == listOfDtosASC

        where:
        sortingType                 |sortingOrder
        SortingType.LIKES            |SortingOrder.ASC
    }

    def "should return a list of movies order by Dislikes in descent order"() {
        given:
        SortingType orderBy = SortingType.DISLIKES

        when:
        Movies moviesReturn = movieServiceImpl.findMovies(sortingType, sortingOrder)

        then:
        1 * movieRepository.findAll(Sort.by(desc, orderBy.getValue())) >> listOfMoviesDESC
        1 * entityConverter.ToDto(listOfMoviesDESC) >> listOfDtosDESC
        0 * _

        and:
        moviesReturn.movies.size() == 2
        moviesReturn.movies == listOfDtosDESC

        where:
        sortingType                 |sortingOrder
        SortingType.DISLIKES          |SortingOrder.DESC
    }

    def "should return a list of movies order by Dislikes in ascent order"() {
        given:
        SortingType orderBy = SortingType.DISLIKES

        when:
        Movies moviesReturn = movieServiceImpl.findMovies(sortingType, sortingOrder)

        then:
        1 * movieRepository.findAll(Sort.by(asc, orderBy.getValue())) >> listOfMoviesASC
        1 * entityConverter.ToDto(listOfMoviesASC) >> listOfDtosASC
        0 * _

        and:
        moviesReturn.movies.size() == 2
        moviesReturn.movies == listOfDtosASC

        where:
        sortingType                 |sortingOrder
        SortingType.DISLIKES            |SortingOrder.ASC
    }
}
