--
-- Sample dataset containing a number of Users and Movies
--
-- =================================================================================================

--USER
INSERT INTO user (user_id,username,password,active,role) VALUES (1,'Spock','pass1234',true,'USER');
INSERT INTO user (user_id,username,password,active,role) VALUES (2,'Princess Amidala','pass1234',true,'USER');
INSERT INTO user (user_id,username,password,active,role) VALUES (3,'Picard','pass1234',true,'USER');
INSERT INTO user (user_id,username,password,active,role) VALUES (4,'Count Dooku','pass1234',false,'USER');

--MOVIE
INSERT INTO movie (movie_id,title,description,user,likes,dislikes,version, created_at) VALUES (1,'Batman: The dark knight rises','I am Batman',1,2,0,0, '2019-01-01');
INSERT INTO movie (movie_id,title,description,user,likes,dislikes,version, created_at) VALUES (2,'Superman: The man of steel','I am Superman',2,1,1,0, '2019-02-02');
INSERT INTO movie (movie_id,title,description,user,likes,dislikes,version, created_at) VALUES (3,'Spiderman Homecoming','I am Spider-man',3,1,1,0, '2019-03-03');
INSERT INTO movie (movie_id,title,description,user,likes,dislikes,version, created_at) VALUES (4,'Star Trek: The final frontier','Live long and prosper',1,1,1,0, '2019-04-04');
INSERT INTO movie (movie_id,title,description,user,likes,dislikes,version, created_at) VALUES (5,'Star Wars: Empire strikes back','Luke you are my son',2,1,1,0, '2019-05-05');
INSERT INTO movie (movie_id,title,description,user,likes,dislikes,version, created_at) VALUES (6,'The legend of Zoro','Z',3,1,1,0, '2019-06-06');
INSERT INTO movie (movie_id,title,description,user,likes,dislikes,version, created_at) VALUES (7,'Casa de pappel','Dali masks and red suits',3,0,3,0, '2019-07-07');

--VOTE
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,1,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,2,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,3,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,4,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,5,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,6,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (1,3,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (1,6,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,1,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,2,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,3,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,4,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,5,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,6,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (3,6,'DOWNVOTE');