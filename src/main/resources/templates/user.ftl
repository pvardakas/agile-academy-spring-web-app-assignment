<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Movies</title>
    <link rel="stylesheet" href="../css/style.css">
    <#include "nav/navbar.ftl">
    <#include "nav/sidebar.ftl">
</head>
<body id="movies-body-background">
<div id="logged-user-info">Logged in as: ${userDto.username}</div>
<span style="font-size:2.2vw;cursor:pointer;color:white" onclick="openNav()">&#9776; Menu</span>


<div class="user-page-container">
    <div class="header">Create a new movie</div>
    <div class="info">In the form below you can submit a new movie</div>
    <form class="user-form-movie-input" action="/user/" method=POST>

        <fieldset class="formContainer">
            <div>
                <label for="title"><b>Title</b></label>
                <@spring.formInput path="movieForm.title" attributes="class='form-inline'"/>
                <@spring.showErrors "<br>", "color:red"/>
            </div>
           <div>
               <label for="description"><b>Description</b></label>
               <@spring.formInput path="movieForm.description" attributes="class='form-inline'"/>
               <@spring.showErrors "<br>", "color:red"/>
           </div>
            <button type="submit">Save</button>
        </fieldset>
    </form>

    <div class="movie-table-container">
        <#if movieDtoList??>
            <div>
                <table id="movieTable" cellspacing="0">
                    <tr class="movie-table-headers">
                        <th class="th-ti">Title
                        </th>
                        <th class="th-de">Description
                        </th>
                        <th class="th-button">Likes
                            <form method="GET" action="/user/">
                                <input type="hidden" name="sortingType" value="LIKES"/>
                                <input type="hidden" name="sortingOrder" value="ASC"/>
                                <input class="movie-submit-button" type="submit" value=&#x2912; />
                            </form>
                            <form method="GET" action="/user/">
                                <input type="hidden" name="sortingType" value="LIKES"/>
                                <input type="hidden" name="sortingOrder" value="DESC"/>
                                <input class="movie-submit-button" type="submit" value=&#x2913; />
                            </form>
                        </th>
                        <th class="th-button">Dislikes
                            <form method="GET" action="/user/">
                                <input type="hidden" name="sortingType" value="DISLIKES"/>
                                <input type="hidden" name="sortingOrder" value="ASC"/>
                                <input class="movie-submit-button" type="submit" value=&#x2912; />
                            </form>
                            <form method="GET" action="/user/">
                                <input type="hidden" name="sortingType" value="DISLIKES"/>
                                <input type="hidden" name="sortingOrder" value="DESC"/>
                                <input class="movie-submit-button" type="submit" value=&#x2913; />
                            </form>
                        </th>
                        <th class="th-button">Publication Date
                            <form method="GET" action="/user/">
                                <input type="hidden" name="sortingType" value="DATE"/>
                                <input type="hidden" name="sortingOrder" value="ASC"/>
                                <input class="movie-submit-button" type="submit" value=&#x2912; />
                            </form>
                            <form method="GET" action="/user/">
                                <input type="hidden" name="sortingType" value="DATE"/>
                                <input type="hidden" name="sortingOrder" value="DESC"/>
                                <input class="movie-submit-button" type="submit" value=&#x2913; />
                            </form>
                        </th>
                    </tr>
                    <tbody>
                    <#list movieDtoList as movie>
                    <tr>
                        <td> ${movie.title}</td>
                        <td> ${movie.description}</td>
                        <td> ${movie.likes}</td>
                        <td> ${movie.dislikes}</td>
                        <td> ${movie.publicationDate}</td>
                    </tr>
                    </tbody>
                    </#list>
                </table>
            </div>
        <#else>
            <div id="movie-submition-info" align="center"> info: ${userDto.username} have not submitted any movies!</div>
        </#if>
    </div>

    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "200px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
    </script>
</div>
</div>
</body>
</html>