<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Movies</title>
    <link rel="stylesheet" href="../css/style.css">
    <#include "nav/navbar.ftl">
</head>

<body id="movies-body-background">

<div class="movies-container">
    <header class="header">Movie List</header>
    <p class="info">Browse here our great collection of movies. You can arrange the list by likes, dislikes or
        date!</p>
    <div class="main-movies-container">
    <#if movieDtoList??>
        <table id="movieTable" cellspacing="0">
            <tr class="movie-table-headers">
                <th class="th-ti">Title
                </th>
                <th class="th-de">Description
                </th>
                <th class="th-us">User
                </th>
                <th class="th-button">Likes
                    <form method="GET" action="/movies">
                        <input type="hidden" name="sortingType" value="LIKES"/>
                        <input type="hidden" name="sortingOrder" value="ASC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2912; />
                    </form>
                    <form method="GET" action="/movies">
                        <input type="hidden" name="sortingType" value="LIKES"/>
                        <input type="hidden" name="sortingOrder" value="DESC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2913; />
                    </form>
                </th>
                <th class="th-button">Dislikes
                    <form method="GET" action="/movies">
                        <input type="hidden" name="sortingType" value="DISLIKES"/>
                        <input type="hidden" name="sortingOrder" value="ASC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2912; />
                    </form>
                    <form method="GET" action="/movies">
                        <input type="hidden" name="sortingType" value="DISLIKES"/>
                        <input type="hidden" name="sortingOrder" value="DESC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2913; />
                    </form>
                </th>
                <th class="th-button">Publication Date
                    <form method="GET" action="/movies">
                        <input type="hidden" name="sortingType" value="DATE"/>
                        <input type="hidden" name="sortingOrder" value="ASC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2912; />
                    </form>
                    <form method="GET" action="/movies">
                        <input type="hidden" name="sortingType" value="DATE"/>
                        <input type="hidden" name="sortingOrder" value="DESC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2913; />
                    </form>
                </th>
            </tr>
            <tbody>
            <#list movieDtoList as movie>
            <tr>
                <td> ${movie.title}</td>
                <td> ${movie.description}</td>
                <td> ${movie.user.getUsername()}</td>
                <td> ${movie.likes}</td>
                <td> ${movie.dislikes}</td>
                <td> ${movie.publicationDate}</td>
            </tr>
            </tbody>
            </#list>
        </table>
    <#else>
        <div id="movie-submition-info" align="center"> info: Users have not submitted any movies!</div>
    </#if>
    </div>
</div>
</body>
</html>