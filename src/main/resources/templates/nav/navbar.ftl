<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
</head>
<body class="top-navigation-bar-body">
<div class="top-navigation-bar">
    <div class="top-navigation-bar-button-position">
        <a href="/">Home</a>
        <a href="/movies">Movies</a>
        <a href="/user">User</a>
        <a href="/login/">Login</a>
    </div>
</div>
</body>
</html>