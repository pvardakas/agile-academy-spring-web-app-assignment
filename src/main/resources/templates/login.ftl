<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login page</title>
    <link rel="stylesheet" href="../css/style.css">
    <#include "nav/navbar.ftl">
    <#import "/spring.ftl" as spring/>
</head>

<body id="signin-form-body">

<div class="login-page-container">
    <div class="header">Please sign in</div>
    <div class="info">Use your username and password to sign in</div>
    <form class="login-form" action="/login" method="post">
        <fieldset class="formContainer">
            <div>
                <label for="info"><b>Username</b></label>
                <@spring.formInput path="userForm.username" attributes="class=form-input" />
                <@spring.showErrors "<br>", "color:red"/>
            </div>
            <div>
                <label for="info"><b>Password</b></label>
                <@spring.formPasswordInput path="userForm.password" attributes="class=form-input" />
                <@spring.showErrors "<br>", "color:red"/>
            </div>
        </fieldset>
        <button class="login-submit-button" type="submit">Login</button>
        <div id="checkbox">
            <input type="checkbox" checked="checked" name="remember"> Remember me
        </div>
        <div class="info">You don't have an account?
            <a href="/register">Create one here</a>.
        </div>
    </form>
</div>
</body>
</html>