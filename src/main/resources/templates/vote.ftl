<!DOCTYPE html>
<html lang="en">

<head>
    <title>Vote Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <#include "nav/navbar.ftl">
    <#include "nav/sidebar.ftl">
</head>

<body id="movies-body-background">
<div id="logged-user-info">Logged in as: ${userDto.username}</div>
<span style="font-size:2.2vw;cursor:pointer;color:white" onclick="openNav()">&#9776; Menu</span>

<div class="user-page-container">
    <div class="header">Vote your favorite movies</div>
    <div class="info">You can vote for your favorite movie either by like or dislike</div>
    <div class="main-movies-container">
        <#if movieDtoList??>
        <table id="movieTable" cellspacing="0">
            <tr class="movie-table-headers">
                <th class="th-ti">Title</th>
                <th class="th-de">Description</th>
                <th class="th-button">Likes
                    <form method="GET" action="/vote/">
                        <input type="hidden" name="sortingType" value="LIKES"/>
                        <input type="hidden" name="sortingOrder" value="ASC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2912; />
                    </form>
                    <form method="GET" action="/vote/">
                        <input type="hidden" name="sortingType" value="LIKES"/>
                        <input type="hidden" name="sortingOrder" value="DESC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2913; />
                    </form>
                </th>
                <th class="th-button">Dislikes
                    <form method="GET" action="/vote/">
                        <input type="hidden" name="sortingType" value="DISLIKES"/>
                        <input type="hidden" name="sortingOrder" value="ASC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2912; />
                    </form>
                    <form method="GET" action="/vote/">
                        <input type="hidden" name="sortingType" value="DISLIKES"/>
                        <input type="hidden" name="sortingOrder" value="DESC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2913; />
                    </form>
                </th>
                <th class="th-button">Publication Date
                    <form method="GET" action="/vote/">
                        <input type="hidden" name="sortingType" value="DATE"/>
                        <input type="hidden" name="sortingOrder" value="ASC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2912; />
                    </form>
                    <form method="GET" action="/vote/">
                        <input type="hidden" name="sortingType" value="DATE"/>
                        <input type="hidden" name="sortingOrder" value="DESC"/>
                        <input class="movie-submit-button" type="submit" value=&#x2913; />
                    </form>
                </th>
                <th class="th-pd">Vote</th>

            </tr>
            <tbody>
            <#list movieDtoList as movie>
            <tr>
                <td> ${movie.title}</td>
                <td> ${movie.description}</td>
                <td> ${movie.likes}</td>
                <td> ${movie.dislikes}</td>
                <td> ${movie.publicationDate}</td>
                <td>
                    <div class="th-button">
                        <form method="POST" action="/vote/">
                            <input type="hidden" name="id" value="${movie.id}"/>
                            <input type="hidden" name="voteType" value="UPVOTE"/>
                            <input class="movie-submit-button" type="submit" value=&#128077; />
                        </form>
                    </div>

                    <div class="th-button">
                        <form method="POST" action="/vote/">
                            <input type="hidden" name="id" value="${movie.id}"/>
                            <input type="hidden" name="voteType" value="DOWNVOTE"/>
                            <input class="movie-submit-button" type="submit" value=&#128078; />
                        </form>
                    </div>
                </td>
            </tbody>
            </#list>
        </table>
    </div>
    <#else>
        <div id="userSubmitionInfo" align="center"> info: ${userDto.username} there are not movies to vote</div>
    </#if>
</div>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "200px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>
</body>
</html>