<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Amdb homepage</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css" >
    <#include "nav/navbar.ftl">
</head>

<body class="index">
<div class="index-body">
    <header class="welcome-header">Welcome to AMDB</header>
    <div class="index-body-info">Here you can find a great collection of movies.
        <br>You can submit your own movie, or you can vote for other.
        <br>Have fun!</div>
    <a class="index-show-movies-button" href="/movies">Browse Movies</a>
</div>
</body>
</html>
