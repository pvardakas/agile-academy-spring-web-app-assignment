<!DOCTYPE html>
<html lang="en">

<head>
    <title>Error!</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<div class="alert">
    <span class="closebtn" onclick="history.back()">&times;</span>
     ${message}
</div>

</body>
</html>