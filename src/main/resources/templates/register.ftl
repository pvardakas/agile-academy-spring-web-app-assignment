<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration Page</title>
    <link rel="stylesheet" href="../css/style.css">
    <#include "nav/navbar.ftl">
    <#import "/spring.ftl" as spring/>
</head>


<body id="movies-body-background">
<div class="login-page-container">
    <div class="header">Create new account</div>
    <div class="info">Please fill in the form below</div>
    <form action="/register" method="post">
        <fieldset class="formContainer">
            <div>
                <label for="info"><b>Username</b></label>
                <@spring.formInput path="userForm.username" attributes="class=form-input" />
                <@spring.showErrors "<br>", "color:red"/>
            </div>
            <div>
                <label for="info"><b>Password</b></label>
                <@spring.formPasswordInput path="userForm.password" attributes="class=form-input" />
                <@spring.showErrors "<br>", "color:red"/>
            </div>
            <div class="containerButtons">
                <button type="submit">Submit</button>
                <button type="reset">Clear</button>
            </div>
        </fieldset>
    </form>
</div>
</body>
</html>