package com.learningactors.academy.amdb.controller;

import com.learningactors.academy.amdb.domain.Movies;
import com.learningactors.academy.amdb.enums.SortingOrder;
import com.learningactors.academy.amdb.enums.SortingType;
import com.learningactors.academy.amdb.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MovieController {

    private MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping(value = "/movies")
    public String findMovies(Model model,
                             @RequestParam(value = "sortingType", required = false) SortingType sortingType,
                             @RequestParam(value = "sortingOrder", required = false) SortingOrder sortingOrder) {
        Movies movieSvr = movieService.findMovies(sortingType, sortingOrder);
        model.addAttribute(movieSvr.getMovies());
        return "movies";
    }
}
