package com.learningactors.academy.amdb.controller;

import com.learningactors.academy.amdb.configuration.LoginSuccessHandler;
import com.learningactors.academy.amdb.domain.Movies;
import com.learningactors.academy.amdb.dto.MovieDto;
import com.learningactors.academy.amdb.dto.UserDto;
import com.learningactors.academy.amdb.dto.VoteDto;
import com.learningactors.academy.amdb.enums.SortingOrder;
import com.learningactors.academy.amdb.enums.SortingType;
import com.learningactors.academy.amdb.enums.VoteType;
import com.learningactors.academy.amdb.exceptions.MovieExistsException;
import com.learningactors.academy.amdb.exceptions.VoteExistsException;
import com.learningactors.academy.amdb.service.MovieService;
import com.learningactors.academy.amdb.service.UserService;
import com.learningactors.academy.amdb.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/vote/")
public class VoteController {
    private UserService userService;
    private MovieService movieService;
    private VoteService voteService;
    private LoginSuccessHandler loginSuccessHandler;

    @Autowired
    public VoteController(UserService userService, MovieService movieService, VoteService voteService, LoginSuccessHandler loginSuccessHandler) {
        this.userService = userService;
        this.movieService = movieService;
        this.voteService = voteService;
        this.loginSuccessHandler = loginSuccessHandler;
    }

    private UserDto getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDto authenticatedUser = loginSuccessHandler.getAuthenticatedUser(authentication);
        return userService.getUserByID(authenticatedUser.getId());
    }


    @GetMapping("")
    public String userVotes(Model model,
                            @RequestParam(value = "sortingType", required = false) SortingType sortingType,
                            @RequestParam(value = "sortingOrder", required = false) SortingOrder sortingOrder) {
        UserDto user = getAuthenticatedUser();
        Movies movies = movieService.findMovies(sortingType, sortingOrder);
        Movies moviesExceptSubmittedByUser = movieService.findMoviesExceptSubmittedByUser(user, movies);
        model.addAttribute(moviesExceptSubmittedByUser.getMovies());
        model.addAttribute(user);
        model.addAttribute("movieForm", new MovieDto());
        return "vote";
    }

    @PostMapping("")
    public String voteMovie(Model model,
                            @RequestParam(value = "id", required = false) String id,
                            @RequestParam(value = "voteType", required = false) VoteType voteType) {
        VoteDto voteDto = new VoteDto();
        voteDto.setMovieId(Long.valueOf(id));
        voteDto.setVoteType(voteType);
        UserDto user = getAuthenticatedUser();
        voteService.save(voteDto, Long.valueOf(user.getId()));
        return "redirect:/vote/";
    }

    @ExceptionHandler(VoteExistsException.class)
    public String error(VoteExistsException ex, Model model) {
        model.addAttribute("message", ex.getMessage());
        return "/error";
    }

    @ExceptionHandler(MovieExistsException.class)
    public String error(MovieExistsException ex, Model model) {
        model.addAttribute("message", ex.getMessage());
        return "/error";
    }
}
