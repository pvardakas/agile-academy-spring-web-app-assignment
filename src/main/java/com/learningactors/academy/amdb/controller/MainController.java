package com.learningactors.academy.amdb.controller;

import com.learningactors.academy.amdb.configuration.LoginSuccessHandler;
import com.learningactors.academy.amdb.dto.UserDto;
import com.learningactors.academy.amdb.exceptions.BadCredentialsException;
import com.learningactors.academy.amdb.exceptions.UserExistsException;
import com.learningactors.academy.amdb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class MainController {

    private UserService userService;
    private LoginSuccessHandler loginSuccessHandler;

    @Autowired
    public MainController(UserService userService, LoginSuccessHandler loginSuccessHandler) {
        this.userService = userService;
        this.loginSuccessHandler = loginSuccessHandler;
    }

    @GetMapping("/")
    public String homePage() {
        return "index";
    }

    @GetMapping("/user")
    public String userPage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDto userDto = loginSuccessHandler.getAuthenticatedUser(authentication);

        if (authentication instanceof AnonymousAuthenticationToken) {
            return "redirect:/login";
        }
        model.addAttribute(userDto);
        return "redirect:/user/";
    }


    @GetMapping("/login")
    public String login(@ModelAttribute(name = "userForm")@Valid UserDto userDto, BindingResult bindingResult, Model model,
            @RequestParam(value = "logout", required = false) String logout) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("userForm", userDto);
        }

        if (logout != null) {
            model.addAttribute("msg", "You've been logged out successfully.");
        }
        return "login";
    }

    @GetMapping("/register")
    public String registerPage(Model model) {
        model.addAttribute("userForm", new UserDto());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute(name = "userForm")@Valid UserDto userDto, BindingResult bindingResult, Model model) {

        if(bindingResult.hasErrors()) {
            model.addAttribute("userForm", userDto);
            return "register";
        }else {
            UserDto savedUser = userService.save(userDto);
            model.addAttribute(savedUser);
            return "login";
        }

    }

    @ExceptionHandler(BadCredentialsException.class)
    public String error(BadCredentialsException ex, Model model) {
        model.addAttribute("message", ex.getMessage());
        return "/error";
    }

    @ExceptionHandler(UserExistsException.class)
    public String error(UserExistsException ex, Model model) {
        model.addAttribute("message", ex.getMessage());
        return "/error";
    }

}
