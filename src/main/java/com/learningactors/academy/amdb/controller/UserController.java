package com.learningactors.academy.amdb.controller;

import com.learningactors.academy.amdb.configuration.LoginSuccessHandler;
import com.learningactors.academy.amdb.domain.Movies;
import com.learningactors.academy.amdb.dto.MovieDto;
import com.learningactors.academy.amdb.dto.UserDto;
import com.learningactors.academy.amdb.enums.SortingOrder;
import com.learningactors.academy.amdb.enums.SortingType;
import com.learningactors.academy.amdb.service.MovieService;
import com.learningactors.academy.amdb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/user/")
public class UserController {

    private UserService userService;
    private MovieService movieService;
    private LoginSuccessHandler loginSuccessHandler;

    @Autowired
    public UserController(MovieService movieService, UserService userService, LoginSuccessHandler loginSuccessHandler) {
        this.movieService = movieService;
        this.userService = userService;
        this.loginSuccessHandler = loginSuccessHandler;
    }

    private UserDto getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDto authenticatedUser = loginSuccessHandler.getAuthenticatedUser(authentication);
        return userService.getUserByID(authenticatedUser.getId());
    }

    @GetMapping("")
    public String userHome(Model model,
                           @RequestParam(value = "sortingType", required = false) SortingType sortingType,
                           @RequestParam(value = "sortingOrder", required = false) SortingOrder sortingOrder) {
        UserDto user = getAuthenticatedUser();
        Movies movies = movieService.findMovies(sortingType, sortingOrder);
        Movies movieByUser = movieService.findMoviesSubmittedByUser(user, movies);
        model.addAttribute(movieByUser.getMovies());
        model.addAttribute(user);
        model.addAttribute("movieForm", new MovieDto());
        return "user";
    }



    @PostMapping("")
    public String saveMovie(@RequestParam(value = "sortingType", required = false) SortingType sortingType,
                            @RequestParam(value = "sortingOrder", required = false) SortingOrder sortingOrder,
                            @ModelAttribute(name = "movieForm")
                            @Valid MovieDto movieDto, BindingResult bindingResult, Model model) {
        UserDto user = getAuthenticatedUser();
        if (bindingResult.hasErrors()) {
            model.addAttribute("movieForm", movieDto);
        } else {
            movieService.save(movieDto, Long.valueOf(user.getId()));
            model.addAttribute("movieForm", new MovieDto());
        }

        Movies movies = movieService.findMovies(sortingType, sortingOrder);
        Movies movieByUser = movieService.findMoviesSubmittedByUser(user, movies);
        model.addAttribute(movieByUser.getMovies());
        model.addAttribute(user);
        return "user";
    }

//TODO
//    @GetMapping("/admin")
//    public String adminHome(Model model) {
//        model.addAttribute(new UserDto());
//        return "/admin";
//    }
}
