package com.learningactors.academy.amdb.exceptions;

public class UserExistsException extends GeneralException{

    public UserExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserExistsException(String message) {
        super(message);
    }
}

