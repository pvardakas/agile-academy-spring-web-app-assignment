package com.learningactors.academy.amdb.exceptions;

public class MovieExistsException extends GeneralException{

    public MovieExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public MovieExistsException(String message) {
        super(message);
    }
}

