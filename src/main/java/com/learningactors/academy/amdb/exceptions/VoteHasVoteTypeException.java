package com.learningactors.academy.amdb.exceptions;

public class VoteHasVoteTypeException extends GeneralException{

    public VoteHasVoteTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public VoteHasVoteTypeException(String message) {
        super(message);
    }
}

