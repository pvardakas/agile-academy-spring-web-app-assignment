package com.learningactors.academy.amdb.exceptions;

public class VoteExistsException extends GeneralException{

    public VoteExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public VoteExistsException(String message) {
        super(message);
    }
}

