package com.learningactors.academy.amdb.exceptions.errors;

public class Error {
    private String message;

    public Error() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public Error(String message) {
        this.message = message;
    }
}
