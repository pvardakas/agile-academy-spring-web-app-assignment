package com.learningactors.academy.amdb.exceptions;

public class BadCredentialsException extends GeneralException{

    public BadCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadCredentialsException(String message) {
        super(message);
    }
}

