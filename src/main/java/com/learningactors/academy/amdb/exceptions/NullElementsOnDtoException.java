package com.learningactors.academy.amdb.exceptions;

public class NullElementsOnDtoException extends GeneralException{
    public NullElementsOnDtoException(String message) {
        super(message);
    }
}
