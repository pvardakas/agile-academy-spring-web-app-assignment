package com.learningactors.academy.amdb.exceptions;

public class UserCantVoteException extends GeneralException{

    public UserCantVoteException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserCantVoteException(String message) {
        super(message);
    }
}

