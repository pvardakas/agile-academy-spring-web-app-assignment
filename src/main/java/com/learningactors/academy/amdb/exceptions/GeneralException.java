package com.learningactors.academy.amdb.exceptions;

public class GeneralException extends RuntimeException{


    public GeneralException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneralException(String message) {
        super(message);
    }
}

