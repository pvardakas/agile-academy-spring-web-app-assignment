package com.learningactors.academy.amdb.exceptions;

public class MovieNotFoundException extends GeneralException {
    public MovieNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
