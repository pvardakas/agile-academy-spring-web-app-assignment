package com.learningactors.academy.amdb.domain;

import java.util.List;

import com.learningactors.academy.amdb.dto.MovieDto;


public class Movies {

    private List<MovieDto> movies;

    public Movies() {
    }

    public List<MovieDto> getMovies() {
        return movies;
    }

    public void setMovies(List<MovieDto> movies) {
        this.movies = movies;
    }
}
