package com.learningactors.academy.amdb.dto;

import com.learningactors.academy.amdb.enums.VoteType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class VoteDto {

    @Enumerated(EnumType.STRING)
    private VoteType voteType;
    private Long movieId;
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public VoteType getVoteType() {
        return voteType;
    }

    public void setVoteType(VoteType voteType) {
        this.voteType = voteType;
    }

}
