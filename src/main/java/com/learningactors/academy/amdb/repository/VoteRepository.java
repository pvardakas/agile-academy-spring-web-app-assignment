package com.learningactors.academy.amdb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.learningactors.academy.amdb.entity.UserMovieKey;
import com.learningactors.academy.amdb.entity.Vote;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository extends JpaRepository<Vote, UserMovieKey> {
    Vote save(Vote vote);

    Vote getVoteById(UserMovieKey voteId);

    void deleteById(UserMovieKey voteId);
}
