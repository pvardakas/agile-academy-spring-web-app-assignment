package com.learningactors.academy.amdb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.learningactors.academy.amdb.entity.Movie;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    Movie save(Movie movie);

    Movie findMovieById(Long id);

    Movie findMovieByTitleAndDescription(String title, String description);

    Movie findMovieByTitle(String title);

}
