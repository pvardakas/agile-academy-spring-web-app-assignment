package com.learningactors.academy.amdb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.learningactors.academy.amdb.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
