package com.learningactors.academy.amdb.converter;

import com.learningactors.academy.amdb.dto.UserDto;
import com.learningactors.academy.amdb.dto.VoteDto;
import com.learningactors.academy.amdb.entity.User;
import com.learningactors.academy.amdb.entity.UserMovieKey;
import com.learningactors.academy.amdb.entity.Vote;
import com.learningactors.academy.amdb.service.MovieService;
import com.learningactors.academy.amdb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VoteConverter {
    private MovieService movieService;
    private UserService userService;
    private UserConverter userConverter;

    @Autowired
    public VoteConverter(MovieService movieService, UserService userService, UserConverter userConverter) {
        this.movieService = movieService;
        this.userService = userService;
        this.userConverter = userConverter;
    }

    public Vote toVote(VoteDto voteDto, Long userId) {
        UserMovieKey userMovieKey = new UserMovieKey();
        userMovieKey.setMovieId(voteDto.getMovieId());
        userMovieKey.setUserId(userId);
        Vote vote = new Vote();
        vote.setId(userMovieKey);
        vote.setVoteType(voteDto.getVoteType());
        UserDto userDto = userService.getUserByID(userId);
        User user = userConverter.toUser(userDto);
        vote.setUser(user);
        vote.setMovie(movieService.findMovieById(voteDto.getMovieId()));
        return vote;
    }

    public VoteDto toVoteDto(Vote vote) {
        VoteDto voteDto = new VoteDto();
        Long movieID = vote.getMovie().getId();
        voteDto.setMovieId(movieID);
        voteDto.setVoteType(vote.getVoteType());
        return voteDto;
    }

}
