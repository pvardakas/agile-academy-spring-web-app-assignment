package com.learningactors.academy.amdb.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.learningactors.academy.amdb.dto.MovieDto;
import com.learningactors.academy.amdb.entity.Movie;

@Component
public class MovieConverter {

    @Autowired
    private UserConverter userConverter;

    public MovieDto toMovieDto(Movie movie) {
        MovieDto dto = new MovieDto();
        dto.setId(movie.getId());
        dto.setTitle(movie.getTitle());
        dto.setDescription(movie.getDescription());
        dto.setLikes(movie.getLikes());
        dto.setDislikes(movie.getDislikes());
        dto.setPublicationDate(movie.getCreatedAt());
        dto.setUser(userConverter.toUserDto(movie.getUser()));
        return dto;
    }

    public Movie toMovie(MovieDto movieDto) {
        Movie movie = new Movie();
        movie.setId(movieDto.getId());
        movie.setTitle(movieDto.getTitle());
        movie.setDescription(movieDto.getDescription());
        movie.setLikes(movieDto.getLikes());
        movie.setDislikes(movieDto.getDislikes());
        return movie;
    }
}
