package com.learningactors.academy.amdb.converter;

import com.learningactors.academy.amdb.dto.MovieDto;
import com.learningactors.academy.amdb.entity.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EntityConverter {
    private MovieConverter movieConverter;

    @Autowired
    public EntityConverter(MovieConverter movieConverter) {
        this.movieConverter = movieConverter;
    }

    public List<MovieDto> ToDto(List<Movie> movies) {
        return movies
                .stream()
                .map(movie -> movieConverter.toMovieDto(movie))
                .collect(Collectors.toList());
    }
}
