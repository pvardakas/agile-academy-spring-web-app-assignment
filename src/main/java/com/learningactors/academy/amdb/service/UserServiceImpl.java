package com.learningactors.academy.amdb.service;

import com.learningactors.academy.amdb.converter.UserConverter;
import com.learningactors.academy.amdb.dto.UserDto;
import com.learningactors.academy.amdb.entity.User;
import com.learningactors.academy.amdb.enums.Role;
import com.learningactors.academy.amdb.exceptions.MovieExistsException;
import com.learningactors.academy.amdb.exceptions.UserExistsException;
import com.learningactors.academy.amdb.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private UserConverter userConverter;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserConverter userConverter, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userConverter = userConverter;
    }

    @Override
    public UserDto getUserByUsername(String username) {
        User byUsername = userRepository.findByUsername(username);
        UserDto userDto = userConverter.toUserDto(byUsername);
        return userDto;
    }

    @Override
    public UserDto getUserByID(Long id) {
        User userById = userRepository.getOne(id);
        UserDto userDto = userConverter.toUserDto(userById);
        return userDto;
    }

    @Override
    public UserDto save(UserDto userDto) {
        User existingUser = userRepository.findByUsername(userDto.getUsername());
        if (existingUser != null) {
            throw new UserExistsException("User Already Exists");
        }
        User user = userConverter.toUser(userDto);
        user.setActive(true);
        user.setRole(Role.USER);
        userRepository.save(user);
        return userConverter.toUserDto(user);
    }
}
