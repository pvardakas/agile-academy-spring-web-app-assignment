package com.learningactors.academy.amdb.service;

import com.learningactors.academy.amdb.dto.VoteDto;
import com.learningactors.academy.amdb.entity.Vote;

public interface VoteService {
    VoteDto save(VoteDto voteDto, Long userId);
    void delete(VoteDto voteDto, Long userId);
}
