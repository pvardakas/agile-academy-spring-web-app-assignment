package com.learningactors.academy.amdb.service;

import com.learningactors.academy.amdb.dto.UserDto;
import com.learningactors.academy.amdb.entity.User;

import java.util.List;

public interface UserService {

    UserDto getUserByUsername(String username);

    UserDto getUserByID(Long id);

    UserDto save(UserDto userDto);


}
