package com.learningactors.academy.amdb.service;

import com.learningactors.academy.amdb.converter.VoteConverter;
import com.learningactors.academy.amdb.dto.VoteDto;
import com.learningactors.academy.amdb.entity.Movie;
import com.learningactors.academy.amdb.entity.Vote;
import com.learningactors.academy.amdb.enums.VoteType;
import com.learningactors.academy.amdb.exceptions.VoteHasVoteTypeException;
import com.learningactors.academy.amdb.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class VoteServiceImpl implements VoteService {

    private VoteConverter voteConverter;
    private VoteRepository voteRepository;



    @Autowired
    public VoteServiceImpl(VoteConverter voteConverter, VoteRepository voteRepository) {
        this.voteConverter = voteConverter;
        this.voteRepository = voteRepository;
    }

    @Override
    @Transactional
    public void delete(VoteDto voteDto, Long userId) {
        Vote vote = voteConverter.toVote(voteDto, userId);
        Vote savedVote = voteRepository.getVoteById(vote.getId());

        if (isVoteValid(vote)|| isVoteEqualToSavedVote(vote, savedVote)) {
            voteRepository.deleteById(vote.getId());
            deleteLikesDislikesOnMovie(vote);

        } else {
            throw new VoteHasVoteTypeException("Vote can't deleted");
        }
    }

    @Override
    @Transactional
    public VoteDto save(VoteDto voteDto, Long userId) {
        Vote vote = voteConverter.toVote(voteDto, userId);
        Vote savedVote = voteRepository.getVoteById(vote.getId());

        if (savedVote == null) {
            setLikesDislikesOnMovie(vote);
            return saveVote(vote, savedVote);
        }

        if (isVoteEqualToSavedVote(vote, savedVote)) {
            voteRepository.deleteById(savedVote.getId());
            deleteLikesDislikesOnMovie(vote);
            return new VoteDto();
        }else {
            updateLikesDislikesOnMovie(vote);
            return saveVote(vote, savedVote);
        }

    }

    private VoteDto saveVote(Vote vote, Vote savedVote) {
        if (savedVote == null) {
            Vote voteSave = voteRepository.save(vote);
            return voteConverter.toVoteDto(voteSave);
        }else {
            savedVote.setVoteType(vote.getVoteType());
            voteRepository.save(savedVote);
            return voteConverter.toVoteDto(vote);
        }
    }

    private boolean isVoteValid(Vote vote) {
        if (vote.getMovie() == null || vote.getVoteType() == null) {
            return false;
        }
        return true;
    }

    private boolean isVoteEqualToSavedVote(Vote vote, Vote savedVote) {
        Long userOnVote = vote.getUser().getId();
        VoteType voteType = vote.getVoteType();
        VoteType savedVoteType = savedVote.getVoteType();
        Long userOnSavedVote = savedVote.getUser().getId();

        if(userOnSavedVote.equals(userOnVote) && savedVoteType.equals(voteType)) {
            return true;
        }
        return false;
    }

    private void setLikesDislikesOnMovie(Vote vote) {
        Movie movie = vote.getMovie();
        if (vote.getVoteType().equals(VoteType.UPVOTE)) {
            addLikes(movie);
        }
        if (vote.getVoteType().equals(VoteType.DOWNVOTE)) {
            addDislikes(movie);
        }
    }

    private void updateLikesDislikesOnMovie(Vote vote) {
        Movie movie = vote.getMovie();
        if (vote.getVoteType().equals(VoteType.UPVOTE)) {
            removeDislikes(movie);
            addLikes(movie);
        }
        if (vote.getVoteType().equals(VoteType.DOWNVOTE)) {
            removeLikes(movie);
            addDislikes(movie);
        }
    }

    private void deleteLikesDislikesOnMovie(Vote vote) {
        Movie movie = vote.getMovie();
        if (vote.getVoteType().equals(VoteType.UPVOTE)) {
            removeLikes(movie);
        }
        if (vote.getVoteType().equals(VoteType.DOWNVOTE)) {
            removeDislikes(movie);
        }
    }

    private void addLikes(Movie movie) {
        movie.setLikes(movie.getLikes() + 1);
    }

    private void addDislikes(Movie movie) {
        movie.setDislikes(movie.getDislikes() + 1);
    }

    private void removeLikes(Movie movie) {
        movie.setLikes(movie.getLikes() - 1);
    }

    private void removeDislikes(Movie movie) {
        movie.setDislikes(movie.getDislikes() - 1);
    }

}
