package com.learningactors.academy.amdb.service;

import com.learningactors.academy.amdb.domain.Movies;
import com.learningactors.academy.amdb.dto.MovieDto;
import com.learningactors.academy.amdb.dto.UserDto;
import com.learningactors.academy.amdb.entity.Movie;
import com.learningactors.academy.amdb.entity.User;
import com.learningactors.academy.amdb.enums.SortingOrder;
import com.learningactors.academy.amdb.enums.SortingType;
import org.springframework.data.domain.Sort;

public interface MovieService {

    Movie findMovieById(Long id);

    Movies findMovies(SortingType sortingType, SortingOrder sortingOrder);

    Movies findMoviesSubmittedByUser(UserDto userDto, Movies movies);

    Movies findMoviesExceptSubmittedByUser(UserDto userDto, Movies movies);

    MovieDto save(MovieDto movieDto, Long id);

    MovieDto findMovieByTitle(String title);
}
