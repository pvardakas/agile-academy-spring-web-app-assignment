package com.learningactors.academy.amdb.service;

import com.learningactors.academy.amdb.converter.EntityConverter;
import com.learningactors.academy.amdb.converter.MovieConverter;
import com.learningactors.academy.amdb.converter.UserConverter;
import com.learningactors.academy.amdb.domain.Movies;
import com.learningactors.academy.amdb.dto.MovieDto;
import com.learningactors.academy.amdb.dto.UserDto;
import com.learningactors.academy.amdb.entity.Movie;
import com.learningactors.academy.amdb.entity.User;
import com.learningactors.academy.amdb.enums.SortingOrder;
import com.learningactors.academy.amdb.enums.SortingType;
import com.learningactors.academy.amdb.exceptions.MovieExistsException;
import com.learningactors.academy.amdb.exceptions.NullElementsOnDtoException;
import com.learningactors.academy.amdb.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {

    private MovieRepository movieRepository;
    private MovieConverter movieConverter;
    private UserService userService;
    private UserConverter userConverter;
    private EntityConverter entityConverter;

    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository, MovieConverter movieConverter, UserService userService, UserConverter userConverter, EntityConverter entityConverter) {
        this.movieRepository = movieRepository;
        this.movieConverter = movieConverter;
        this.userService = userService;
        this.userConverter = userConverter;
        this.entityConverter = entityConverter;
    }

    private Sort.Direction asc = Sort.Direction.ASC;
    private Sort.Direction desc = Sort.Direction.DESC;

    @Override
    public Movie findMovieById(Long id) {
        return movieRepository.findMovieById(id);
    }

    @Override
    public Movies findMovies(SortingType sortingType, SortingOrder sortingOrder) {
        if (sortingType == null && sortingOrder == null) {
            sortingType = SortingType.DATE;
            sortingOrder = SortingOrder.DESC;
        }
        Movies movies = new Movies();

        if (sortingOrder == SortingOrder.DESC) {
            movies.setMovies(
                    entityConverter.ToDto(movieRepository.findAll(
                                    Sort.by(desc, sortingType.getValue()))));
        } else {
            movies.setMovies(
                    entityConverter.ToDto(movieRepository.findAll(
                                    Sort.by(asc, sortingType.getValue()))));
        }
        return movies;
    }

    @Override
    public Movies findMoviesSubmittedByUser(UserDto userDto, Movies movies) {
        List<MovieDto> filtered = movies.getMovies()
                .stream()
                .filter(m -> userDto.getId().equals(m.getUser().getId()))
                .collect(Collectors.toList());
        movies.setMovies(filtered);
        return movies;

    }

    @Override
    public Movies findMoviesExceptSubmittedByUser(UserDto userDto, Movies movies) {
        List<MovieDto> filtered = movies.getMovies()
                .stream()
                .filter(m -> !userDto.getId().equals(m.getUser().getId()))
                .collect(Collectors.toList());
        movies.setMovies(filtered);
        return movies;
    }

    @Override
    public MovieDto save(MovieDto movieDto, Long id) {
        if (movieDto == null) {
            throw new NullElementsOnDtoException("Error on submission data");
        }
        Movie movieCreated = createMovie(movieDto, id);
        Movie savedMovie = movieRepository.findMovieByTitleAndDescription(movieCreated.getTitle(), movieCreated.getDescription());
        if (savedMovie == null) {
            movieRepository.save(movieCreated);
            return movieConverter.toMovieDto(movieCreated);
        } else {
            throw new MovieExistsException("Movie Already Exists");
        }
    }

    private Movie createMovie(MovieDto movieDto, Long id) {
        UserDto userDto = userService.getUserByID(id);
        User user = userConverter.toUser(userDto);
        Movie toMovie = movieConverter.toMovie(movieDto);
        toMovie.setUser(user);
        toMovie.setLikes(0L);
        toMovie.setDislikes(0L);
        return toMovie;
    }

    @Override
    public MovieDto findMovieByTitle(String title) {
        Movie movieByTitle = movieRepository.findMovieByTitle(title);
        return movieConverter.toMovieDto(movieByTitle);
    }
}
