package com.learningactors.academy.amdb.enums;

public enum SortingType {

    LIKES("likes"),
    DISLIKES("dislikes"),
    DATE("createdAt"),
    NONE("none");

    private String value;

    SortingType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
