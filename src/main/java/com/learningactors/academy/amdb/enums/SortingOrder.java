package com.learningactors.academy.amdb.enums;

public enum SortingOrder {
    ASC,
    DESC
}
