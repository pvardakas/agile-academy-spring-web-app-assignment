package com.learningactors.academy.amdb.enums;

public enum Role {
    USER,
    ADMIN
}
