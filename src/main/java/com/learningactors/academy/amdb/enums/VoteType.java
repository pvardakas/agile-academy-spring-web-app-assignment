package com.learningactors.academy.amdb.enums;

public enum VoteType {
    UPVOTE,
    DOWNVOTE
}
