package com.learningactors.academy.amdb.restControllers;

import com.learningactors.academy.amdb.domain.Movies;
import com.learningactors.academy.amdb.dto.MovieDto;
import com.learningactors.academy.amdb.dto.UserDto;
import com.learningactors.academy.amdb.dto.VoteDto;
import com.learningactors.academy.amdb.enums.SortingOrder;
import com.learningactors.academy.amdb.enums.SortingType;
import com.learningactors.academy.amdb.exceptions.*;
import com.learningactors.academy.amdb.exceptions.errors.Error;
import com.learningactors.academy.amdb.service.MovieService;
import com.learningactors.academy.amdb.service.UserService;
import com.learningactors.academy.amdb.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users/")
public class UserRestController {

    private MovieService movieService;
    private UserService userService;
    private VoteService voteService;

    @Autowired
    public UserRestController(MovieService movieService, UserService userService, VoteService voteService) {
        this.movieService = movieService;
        this.userService = userService;
        this.voteService = voteService;
    }

    @GetMapping("{userId}")
    ResponseEntity<Movies> getMovies(@PathVariable("userId") Long id,
                                     @RequestParam(value = "sortingType", required = false) SortingType sortingType,
                                     @RequestParam(value = "sortingOrder", required = false) SortingOrder sortingOrder) {
        UserDto user = userService.getUserByID(id);
        Movies movies = movieService.findMovies(sortingType, sortingOrder);
        Movies moviesByUser = movieService.findMoviesSubmittedByUser(user, movies);
        return ResponseEntity.ok().body(moviesByUser);
    }

    @PostMapping("{userId}/movies")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<MovieDto> createMovie(@PathVariable("userId") Long id,
                                                @RequestBody MovieDto movieDto) {
        MovieDto save = movieService.save(movieDto, id);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @PostMapping("{userId}/votes")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<VoteDto> voteMovie(@PathVariable("userId") Long id,
                                             @RequestBody VoteDto voteDto) {
        VoteDto save = voteService.save(voteDto, id);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @PostMapping("{userId}/votes/delete")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity deleteVote(@PathVariable("userId") Long id,
                                     @RequestBody VoteDto voteDto) {
        voteService.delete(voteDto, id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @ExceptionHandler(UserCantVoteException.class)
    public ResponseEntity handleUserCantVoteException(UserCantVoteException ex) {
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(new Error("User cant vote submitted movie"));
    }

    @ExceptionHandler(VoteExistsException.class)
    public ResponseEntity handleVoteExistsException(VoteExistsException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new Error("User has already voted movie"));
    }

    @ExceptionHandler(MovieExistsException.class)
    public ResponseEntity handleMoveExistsException(MovieExistsException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new Error("Movie already exists in database"));
    }

    @ExceptionHandler(MovieNotFoundException.class)
    public ResponseEntity handleMoveNotFoundException(MovieNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new Error("Requested movie not found"));
    }

    @ExceptionHandler(NullElementsOnDtoException.class)
    public ResponseEntity handleNullDtoException(NullElementsOnDtoException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new Error("Error on submitted data"));
    }

    @ExceptionHandler(VoteHasVoteTypeException.class)
    public ResponseEntity handleVoteDeleteException(VoteHasVoteTypeException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new Error("Vote can't delete"));
    }

}

