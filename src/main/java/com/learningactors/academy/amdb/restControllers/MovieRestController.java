package com.learningactors.academy.amdb.restControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.learningactors.academy.amdb.domain.Movies;
import com.learningactors.academy.amdb.enums.SortingOrder;
import com.learningactors.academy.amdb.enums.SortingType;
import com.learningactors.academy.amdb.service.MovieService;

@RestController
@RequestMapping("/api/movies/")
public class MovieRestController {

    private MovieService movieService;

    @Autowired
    public MovieRestController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping()
    public ResponseEntity<Movies> getMovies(@RequestParam(value = "sortingType", required = false) SortingType sortingType,
                                            @RequestParam(value = "sortingOrder", required = false) SortingOrder sortingOrder) {

        Movies movies = movieService.findMovies(sortingType, sortingOrder);
        return ResponseEntity.ok().body(movies);
    }
}
